package dam.androidsaulruiz.u3t4event;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import static java.util.Calendar.getInstance;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener,
        RadioGroup.OnCheckedChangeListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    //Todo Ejercicio1 hacemos un string para la antigua informacion y el Array de Strings de meses
    private String priority = "Normal";
    private String lastDate = "";
    private String[] month = null;
    private String[] datos = null;
    private String[] aux = null;
    private String[] aux2 = null;

    //TODO Ejercicio4 creamos las variables
    private String months = "";
    private String day = "";
    private String year = "";
    private String hour = "";
    private String minute = "";

    private int año, mes, dia, hora, minutos;
    private boolean editar = false;

    private Calendar cal;

    private EditText etPlace;
    private TextView tvEventName;
    private RadioGroup rgPriority;
    private Button btAccept;
    private Button btCancel;
    private Button btDatePicker;
    private Button btTimePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        Bundle inputData = getIntent().getExtras();

        //Todo Ejercicio1 la metemos del bundle
        tvEventName.setText(inputData.getString("EventName"));
        lastDate = inputData.getString("Data");

        //TODO Ejercicio4 inicializamos el calendar
        cal = new Calendar() {
            @Override
            protected void computeTime() {

            }

            @Override
            protected void computeFields() {

            }

            @Override
            public void add(int field, int amount) {

            }

            @Override
            public void roll(int field, boolean up) {

            }

            @Override
            public int getMinimum(int field) {
                return 0;
            }

            @Override
            public int getMaximum(int field) {
                return 0;
            }

            @Override
            public int getGreatestMinimum(int field) {
                return 0;
            }

            @Override
            public int getLeastMaximum(int field) {
                return 0;
            }
        };

        //Todo Ejercicio1 cuando volvemos a dar en editar se mete la informacion antigua
        if (!lastDate.equalsIgnoreCase(getString(R.string.tvCurrentDataMain))) {
            datos = lastDate.split("\n");
            editar = true;

            for (int i = 0; i < datos.length; i++) {
                aux = datos[i].split(" ");
                if (aux.length > 1) {

                    if (i == 0) etPlace.setText(aux[1]);
                    if (i == 1) {
                        priority = aux[1];
                        switch (aux[1]) {
                            case "Low":
                                rgPriority.check(R.id.rbLow);
                                break;
                            case "Normal":
                                rgPriority.check(R.id.rbNormal);
                                break;
                            case "High":
                                rgPriority.check(R.id.rbHigh);
                                break;
                        }
                    }
                    if (i == 2) {
                        for (int j = 0; j < month.length; j++) {
                            //TODO Ejercicio4 guardamos las fechas anteriores en sus variables
                            if (month[j].equals(aux[2])) {
                                año = Integer.parseInt(aux[3]);
                                mes = j;
                                dia = Integer.parseInt(aux[1]);
                            }
                        }
                    }
                    //TODO Ejercicio4 guardamos las fechas anteriores en sus variables
                    if (i == 3) {
                        aux2 = aux[1].split(":");
                        hora = Integer.parseInt(aux2[0]);
                        minutos = Integer.parseInt(aux2[1]);
                    }
                }
            }
            cal.set(año, mes, dia, hora, minutos);
        }
    }

    private void setUI() {
        tvEventName = findViewById(R.id.tvEventName);
        rgPriority = findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);

        //Todo Ejercicio1 le ponemos el array de strings
        month = getResources().getStringArray(R.array.months);

        etPlace = findViewById(R.id.etPlace);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);
        btDatePicker = findViewById(R.id.btDatePicker);
        btTimePicker = findViewById(R.id.btTimePicker);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        btDatePicker.setOnClickListener(this);
        btTimePicker.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
    }


    @Override
    public void onClick(View v) {
        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        switch (v.getId()) {
            case R.id.btAccept:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    eventData.putString("EventData", "PLACE: " + etPlace.getText().toString() + "\n" +
                            "PRIORITY: " + priority + "\n" +
                            "DATE: " + day + " " + months + " " + year + "\n" +
                            "HOUR: " + hour + ":" + minute);
                }
                activityResult.putExtras(eventData);
                setResult(RESULT_OK, activityResult);

                finish();
                break;
            //Todo Ejercicio1 si le damos al cancel le pasamos la anterior info
            case R.id.btCancel:
                eventData.putString("EventData", lastDate);
                activityResult.putExtras(eventData);
                setResult(RESULT_OK, activityResult);

                finish();
                break;
            //TODO Ejercicio4 hacemos el datepickerdialog y si es editado nos pondra la informacion anterior
            case R.id.btDatePicker:
                if (editar) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(this, this,
                            cal.get(Calendar.YEAR),
                            cal.get(Calendar.MONTH),
                            cal.get(Calendar.DAY_OF_MONTH));
                    datePickerDialog.show();
                } else {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(this, this,
                            getInstance().get(Calendar.YEAR),
                            getInstance().get(Calendar.MONTH),
                            getInstance().get(Calendar.DAY_OF_MONTH));
                    datePickerDialog.show();
                }
                break;
            //TODO Ejercicio4 hacemos el timepickerdialog y si es editado nos pondra la informacion anterior
            case R.id.btTimePicker:
                if (editar) {
                    TimePickerDialog timePickerDialog = new TimePickerDialog(this, this,
                            cal.get(Calendar.HOUR_OF_DAY),
                            cal.get(Calendar.MINUTE), true);
                    timePickerDialog.show();
                } else {
                    TimePickerDialog timePickerDialog = new TimePickerDialog(this, this,
                            getInstance().get(Calendar.HOUR_OF_DAY),
                            getInstance().get(Calendar.MINUTE), true);
                    timePickerDialog.show();
                }
                break;
        }
    }


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.rbLow:
                priority = "Low";
                break;
            case R.id.rbNormal:
                priority = "Normal";
                break;
            case R.id.rbHigh:
                priority = "High";
                break;
        }
    }

    //TODO Ejercicio4 cogemos los datos del dialog y se lo pasamos a las variables
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        this.day = "" + dayOfMonth;
        this.months = this.month[month];
        this.year = "" + year;

    }

    //TODO Ejercicio4 cogemos los datos del dialog y se lo pasamos a las variables
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        this.hour = "" + hourOfDay;
        this.minute = "" + minute;
    }
}
