package dam.androidsaulruiz.u3t4event;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    //Todo Ejercicio1 hacemos un string para la antigua informacion y el Array de Strings de meses
    private String priority = "Normal";
    private String lastDate = "";
    private String[] month = null;
    private String[] datos = null;
    private String[] aux = null;
    private String[] aux2 = null;

    private EditText etPlace;
    private TextView tvEventName;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        Bundle inputData = getIntent().getExtras();

        //Todo Ejercicio1 la metemos del bundle
        tvEventName.setText(inputData.getString("EventName"));
        lastDate = inputData.getString("Data");

        //Todo Ejercicio1 cuando volvemos a dar en editar se mete la informacion antigua
        if (!lastDate.equals(getString(R.string.tvCurrentDataMain))) {
            datos = lastDate.split("\n");

            for (int i = 0; i < datos.length; i++) {
                aux = datos[i].split(" ");
                if (aux.length > 1) {
                    Log.i("Testeo", aux[1]);
                    Log.i("Testeo", datos[3]);

                    if (i == 0) etPlace.setText(aux[1]);
                    if (i == 1) {
                        priority = aux[1];
                        switch (aux[1]) {
                            case "Low":
                                rgPriority.check(R.id.rbLow);
                                break;
                            case "Normal":
                                rgPriority.check(R.id.rbNormal);
                                break;
                            case "High":
                                rgPriority.check(R.id.rbHigh);
                                break;
                        }
                    }
                    if (i == 2) {
                        for (int j = 0; j < month.length; j++) {
                            if (month[j].equals(aux[2])) {

                                dpDate.updateDate(Integer.parseInt(aux[3]), j, Integer.parseInt(aux[1]));
                            }
                        }
                    }
                    if (i == 3) {
                        aux2 = aux[1].split(":");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            tpTime.setHour(Integer.parseInt(aux2[0]));
                            tpTime.setMinute(Integer.parseInt(aux2[1]));
                        }
                    }


                }
            }
        }

        //Todo Ejercicio2 si esta en portrait lo ponemos a false y si esta en landscape lo ponemos a true
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ){
            dpDate.setCalendarViewShown(false);
        }else{
            dpDate.setCalendarViewShown(true);
        }
    }

    private void setUI() {
        tvEventName = findViewById(R.id.tvEventName);
        rgPriority = findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);

        //Todo Ejercicio1 le ponemos el array de strings
        month = getResources().getStringArray(R.array.months);

        etPlace = findViewById(R.id.etPlace);
        dpDate = findViewById(R.id.dpDate);
        tpTime = findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
    }


    @Override
    public void onClick(View v) {
        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        switch (v.getId()) {
            case R.id.btAccept:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    eventData.putString("EventData", "PLACE: " + etPlace.getText().toString() + "\n" +
                            "PRIORITY: " + priority + "\n" +
                            "DATE: " + dpDate.getDayOfMonth() + " " + month[dpDate.getMonth()] + " " + dpDate.getYear() + "\n" +
                            "HOUR: " + tpTime.getHour() + ":" + tpTime.getMinute());
                }
                break;
            //Todo Ejercicio1 si le damos al cancel le pasamos la anterior info
            case R.id.btCancel:
                eventData.putString("EventData", lastDate);
                break;
        }
        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.rbLow:
                priority = "Low";
                break;
            case R.id.rbNormal:
                priority = "Normal";
                break;
            case R.id.rbHigh:
                priority = "High";
                break;
        }
    }
}
